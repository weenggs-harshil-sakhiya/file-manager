import { useEffect, useState } from "react";
import { faPlus } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Tooltip } from "antd";

const SelectOptions = ({
  defaultOptions,
  options,
  addFunction,
  onClick,
  selectedOption,
  active,
}) => {
  const [localData, setLocalData] = useState(
    JSON.parse(localStorage.getItem("data"))
  );

  useEffect(() => {
    localStorage.setItem("data", JSON.stringify(localData));
  }, [localData]);

  const onDragStart = (evt) => {
    let element = evt.currentTarget;
    element.classList.add("dragged");
    evt.dataTransfer.setData("text/plain", evt.currentTarget.id);
    evt.dataTransfer.effectAllowed = "move";
  };

  const onDragEnd = (evt) => {
    evt.currentTarget.classList.remove("dragged");
  };

  const onDragEnter = (evt) => {
    evt.preventDefault();
    let element = evt.currentTarget;
    element.classList.add("dragged-over");
    evt.dataTransfer.dropEffect = "move";
  };

  const onDragLeave = (evt) => {
    let currentTarget = evt.currentTarget;
    let newTarget = evt.relatedTarget;
    if (newTarget.parentNode === currentTarget || newTarget === currentTarget)
      return;
    evt.preventDefault();
    let element = evt.currentTarget;
    element.classList.remove("dragged-over");
  };

  const onDragOver = (evt) => {
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "move";
  };

  const onDrop = (evt, value, status) => {
    evt.preventDefault();
    evt.currentTarget.classList.remove("dragged-over");
    let data = evt.dataTransfer.getData("text/plain");
    let tasks = localData;
    console.log(data, status);
    let updated = tasks.map((task) => {
      if (task.id.toString() === data.toString()) {
        task.parentValue = status;
      }
      return task;
    });

    setLocalData(updated);
  };

  return (
    <>
      <ul className="py-[18px] md:pt-[18px] pt-9 pl-[18px] flex flex-col gap-2.5">
        {defaultOptions &&
          defaultOptions?.map((selectOption, key) => (
            <li onDrop={(e) => onDrop(e, false, selectOption.value)}>
              <div
                className={` items-center rounded-l-lg hover:bg-[linear-gradient(90deg,#3f4c653d_24%,#3f4c6500_100%)] dark:hover:bg-[linear-gradient(90deg,#343f54_24%,#1e2732_100%)] ${
                  selectOption?.value === active
                    ? "bg-[linear-gradient(90deg,#3f4c653d_24%,#3f4c6500_100%)] dark:bg-[linear-gradient(90deg,#343f54_24%,#1e2732_100%)]"
                    : ""
                }`}
                key={key}
                onDragLeave={(e) => onDragLeave(e)}
                onDragEnter={(e) => onDragEnter(e)}
                onDragEnd={(e) => onDragEnd(e)}
                onDragOver={(e) => onDragOver(e)}
                onDrop={(e) => onDrop(e, false, selectOption.value)}
              >
                <Button
                  variant="default"
                  className="w-full !border-0 !h-full rounded-none rounded-l-lg text-primary-900 dark:text-white/90 font-normal capitalize py-4 pl-3.5 !pr-0 flex items-center gap-2.5 !bg-transparent hover:!bg-transparent hover:!shadow-none"
                  onClick={() => onClick(selectOption)}
                >
                  <div className="w-[30px] flex justify-center text-primary-900/60 dark:text-white/60">
                    {selectOption?.icon}
                  </div>

                  {selectOption?.name}
                </Button>
              </div>
              <div
                onDragLeave={(e) => onDragLeave(e)}
                onDragEnter={(e) => onDragEnter(e)}
                onDragEnd={(e) => onDragEnd(e)}
                onDragOver={(e) => onDragOver(e)}
                onDrop={(e) => onDrop(e, false, selectOption.value)}
              >
                {localData.length > 0 &&
                  localData
                    .filter((obj) => obj.parentValue === selectOption.value)
                    .map((item) => (
                      <li>
                        <Button
                          key={item.id}
                          id={item.id}
                          variant="default"
                          className="w-full !border-0 !h-full rounded-none rounded-l-lg text-primary-900 dark:text-white/90 font-normal capitalize py-4 pl-3.5 !pr-0 flex items-center gap-2.5 !bg-transparent hover:!bg-transparent hover:!shadow-none"
                          draggable
                          onDragStart={(e) => onDragStart(e)}
                          onDragEnd={(e) => onDragEnd(e)}
                        >
                          {item.folderName}
                        </Button>
                      </li>
                    ))}
              </div>
              {/* {selectOption?.addButton &&
                selectOption?.value === selectedOption && (
                  <Tooltip
                    content={`Add ${selectOption?.name}`}
                    placement="top"
                  >
                    <Button
                      htmlType="button"
                      className={`w-10 !rounded-none h-[52px] min-h-[52px] hover:!bg-transparent active:!bg-transparent`}
                      variant="text"
                      onClick={addFunction}
                    >
                      <FontAwesomeIcon
                        className="text-base w-5 h-5 text-[#343a40]"
                        icon={faPlus}
                      />
                    </Button>
                  </Tooltip>
                )} */}
            </li>
          ))}
      </ul>
    </>
  );
};

export default SelectOptions;
