import React, { useEffect, useState } from "react";
import { faFile } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Typography } from "antd";

const EquipmentItems = ({ data }) => {
  const [localData, setLocalData] = useState(
    JSON.parse(localStorage.getItem("data"))
  );

  useEffect(() => {
    localStorage.setItem("data", JSON.stringify(localData));
  }, [localData]);
  console.log({ localData });

  const onDragStart = (evt) => {
    let element = evt.currentTarget;
    element.classList.add("dragged");
    evt.dataTransfer.setData("text/plain", evt.currentTarget.id);
    evt.dataTransfer.effectAllowed = "move";
  };

  const onDragEnd = (evt) => {
    evt.currentTarget.classList.remove("dragged");
  };

  const onDragEnter = (evt) => {
    evt.preventDefault();
    let element = evt.currentTarget;
    element.classList.add("dragged-over");
    evt.dataTransfer.dropEffect = "move";
  };

  const onDragLeave = (evt) => {
    let currentTarget = evt.currentTarget;
    let newTarget = evt.relatedTarget;
    if (newTarget.parentNode === currentTarget || newTarget === currentTarget)
      return;
    evt.preventDefault();
    let element = evt.currentTarget;
    element.classList.remove("dragged-over");
  };

  const onDragOver = (evt) => {
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "move";
  };

  const onDrop = (evt, value, status) => {
    evt.preventDefault();
    evt.currentTarget.classList.remove("dragged-over");
    let data = evt.dataTransfer.getData("text/plain");
    let tasks = localData;
    console.log(data, status);
    let updated = tasks.map((task) => {
      if (task.id.toString() === data.toString()) {
        task.parentValue = status;
      }
      return task;
    });

    setLocalData(updated);
  };

  return (
    <div className="mt-5">
      <Typography
        title="h5"
        className="md:!text-xl !text-lg !text-primary-900 font-bold        "
      >
        {data.name}
      </Typography>
      {localData.filter((obj) => obj.parentValue === data.value).length > 0 ? (
        <div
          className="flex flex-wrap"
          onDragLeave={(e) => onDragLeave(e)}
          onDragEnter={(e) => onDragEnter(e)}
          onDragEnd={(e) => onDragEnd(e)}
          onDragOver={(e) => onDragOver(e)}
          onDrop={(e) => onDrop(e, false, data.value)}
        >
          {localData
            .filter((obj) => obj.parentValue === data.value)
            .map((item) => {
              return (
                <div
                  key={item.id}
                  id={item.id}
                  className="mt-5 p-5 "
                  draggable
                  onDragStart={(e) => onDragStart(e)}
                  onDragEnd={(e) => onDragEnd(e)}
                >
                  <FontAwesomeIcon
                    className="w-24 h-24 text-primary-900 dark:text-white/90"
                    icon={faFile}
                  />
                  <p className="text-xl font-semibold text-center">
                    {item.folderName}
                  </p>
                </div>
              );
            })}
        </div>
      ) : (
        <p
          onDragLeave={(e) => onDragLeave(e)}
          onDragEnter={(e) => onDragEnter(e)}
          onDragEnd={(e) => onDragEnd(e)}
          onDragOver={(e) => onDragOver(e)}
          onDrop={(e) => onDrop(e, false, data.value)}
          className="flex justify-center items-center h-[60vh] text-3xl font-bold"
        >
          No File
        </p>
      )}
    </div>
  );
};

export default EquipmentItems;
