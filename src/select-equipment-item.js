import { useState } from "react";
import {
  faBars,
  faPlus,
  faScrewdriverWrench,
  faXmark,
} from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRightArrowLeft,
  faDatabase,
  faFileUser,
  faIdBadge,
} from "@fortawesome/pro-solid-svg-icons";
import EquipmentItems from "./EquipmentItems";
import SelectOptions from "./select-options";
import Typography from "antd/es/typography/Typography";
import { Tooltip, Button } from "antd";
import Search from "antd/es/input/Search";
import AddModel from "./AddModel";

export default function SelectEquipmentItemSide({ addItemOpen, options }) {
  const [sideMenuOpen, setSideMenuOpen] = useState(false);
  const [addNewItemOpen, setAddNewItemOpensds] = useState(selectOptions[0]);
  const [addmodel, setAddmodel] = useState(false);

  return (
    <>
      <div className="sidebar-body">
        <div className="flex">
          <div
            className={`w-60 md:min-w-[240px] md:max-w-[240px] md:relative absolute flex-[1_0_0%] z-20 ${
              sideMenuOpen ? "h-screen w-full" : "md:h-screen md:w-full w-0 h-0"
            }`}
          >
            <div
              className={`md:hidden block absolute bg-black/20 rounded-t-lg ${
                sideMenuOpen ? "h-full w-full" : "h-0 w-0"
              }`}
              onClick={() => setSideMenuOpen(false)}
            ></div>
            <div
              className={`overflow-y-auto h-screen relative w-[240px] md:bg-gray-200/50 bg-gray-200 dark:bg-dark-800 transition-all ease-in-out duration-300 ${
                sideMenuOpen ? "left-0" : "md:left-0 -left-[100%]"
              }`}
            >
              <SelectOptions
                defaultOptions={selectOptions}
                options={options}
                selectedOption={addItemOpen}
                onClick={(value) => {
                  setAddNewItemOpensds(value);
                }}
                active={addNewItemOpen.value}
              />
            </div>
          </div>
          <div className="flex w-full  flex-[1_0_0%] overflow-hidden flex-col">
            <div className="px-5 py-2.5 border-b border-gray-300 flex items-center relative md:justify-start justify-center">
              <Button
                htmlType="button"
                className={`md:hidden flex w-6 max-w-[24px] max-h-[24px] !absolute left-2.5`}
                variant="text"
                onClick={() => setSideMenuOpen((prev) => !prev)}
              >
                <FontAwesomeIcon
                  className="text-base w-[18px] h-[18px] text-[#343a40]"
                  icon={faBars}
                />
              </Button>
              <div className="flex items-center text-primary-900 dark:text-white/90">
                <div className="flex items-center justify-center rounded-full mr-2.5 w-11 h-11 bg-gray-200/50 dark:bg-[#333D49]">
                  <FontAwesomeIcon
                    className="w-5 h-5 text-primary-900 dark:text-white/90"
                    icon={faScrewdriverWrench}
                  />
                </div>
                <Typography
                  title="h5"
                  className="md:!text-xl !text-lg !text-primary-900 dark:!text-white/90"
                >
                  {addNewItemOpen.name}
                </Typography>
              </div>
              <div className="flex items-center gap-2">
                <Tooltip content="Add Equipment Item" placement="bottom">
                  <Button
                    htmlType="button"
                    className={`w-6 max-w-[24px] max-h-[24px] md:relative md:right-14 !absolute right-[34px]`}
                    variant="text"
                    onClick={() => setAddmodel(true)}
                  >
                    <FontAwesomeIcon
                      className={`text-base w-[18px] h-[18px] !text-primary-800`}
                      icon={faPlus}
                    />
                  </Button>
                </Tooltip>
                <Tooltip content="Close" placement="bottom">
                  <Button
                    htmlType="button"
                    className={`w-6 max-w-[24px] max-h-[24px] md:relative md:right-6 !absolute right-2.5`}
                    variant="text"
                    onClick={() => setAddmodel("")}
                  >
                    <FontAwesomeIcon
                      className={`text-base w-[18px] h-[18px] !text-primary-800`}
                      icon={faXmark}
                    />
                  </Button>
                </Tooltip>
              </div>
            </div>
            <div className="fixed bottom-14 right-4 w-10 h-10 md:hidden block ">
              <div className="absolute bottom-0 right-0">
                <Button
                  shape="round"
                  htmlType="button"
                  className={`w-10 p-0 !bg-primary-900 max-w-[40px] min-h-[40px]`}
                  variant="text"
                  onClick={() => setAddmodel(true)}
                >
                  <FontAwesomeIcon
                    className="text-base w-5 h-5 text-white"
                    icon={faPlus}
                  />
                </Button>
              </div>
            </div>
            <div className="py-3 px-[15px]">
              <Search
                placeholder="Search for Equipment"
                beforeName=""
                className="border border-solid border-gray-200 focus:!border-primary-900 hover:!border-primary-900 focus-within:!border-primary-900"
              />

              <EquipmentItems data={addNewItemOpen} />
            </div>
          </div>
        </div>
      </div>
      {addmodel && (
        <AddModel
          showModel={(value) => setAddmodel(value)}
          title={addNewItemOpen}
        />
      )}
    </>
  );
}

const localData = JSON.parse(localStorage.getItem("data"));

const selectOptions = [
  {
    id: 1,
    name: "All",
    value: "all",
    icon: (
      <FontAwesomeIcon
        className="w-5 h-5 text-primary-900/60 dark:text-white/90"
        icon={faIdBadge}
      />
    ),
    children: localData
      ? localData?.filter((folder) => folder.parentValue === "all")
      : [],
  },
  {
    id: 2,
    name: "Estimate Items",
    value: "estimate",
    icon: (
      <FontAwesomeIcon
        className="w-5 h-5 text-primary-900/60 dark:text-white/90"
        icon={faFileUser}
      />
    ),
    children: localData
      ? localData?.filter((folder) => folder.parentValue === "estimate")
      : [],
  },
  {
    id: 3,
    name: "Change Order Items",
    value: "change_order",
    icon: (
      <FontAwesomeIcon
        className="w-5 h-5 text-primary-900/60 dark:text-white/90"
        icon={faArrowRightArrowLeft}
      />
    ),
    children: localData
      ? localData?.filter((folder) => folder.parentValue === "change_order")
      : [],
  },
  {
    id: 4,
    name: "Cost Items Database",
    value: "cost_database",
    icon: (
      <FontAwesomeIcon
        className="w-5 h-5 text-primary-900/60 dark:text-white/90"
        icon={faDatabase}
      />
    ),
    children: localData
      ? localData.filter((folder) => folder?.parentValue === "cost_database")
      : [],
  },
];
